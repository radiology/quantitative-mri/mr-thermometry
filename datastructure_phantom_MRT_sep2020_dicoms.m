% Data structure for multi-channel phantom experiments from 2020 with asset:
%
% All data is DICOM format
%
% Supplies information about scan in fields: 
%   filename
%   foldername
%   displayname
%   nechoes (2 for DE and 11 for MEFGRE, 15 for IDEAL IQ)
%   dimensions (2D or 3D)
%
% all that information is stored in allfn and can be fed into import data
% functions
%
%   scanidx: 1-2 ordered by time of acquisition ie always:
%       1 = MEFGRE 
%       2 = IDEAL IQ
%
%
% 14-2-2021, T.Feddersen: created
% 13-3-2023, T. Feddersen edited to include thresholds 

% fatmap_threshold = 1000;

%% General structure of scan session:
scanfields = { 'filename', 'foldername', 'displayname', 'nechoes', 'dimensions', 'fatmap_threshold' }; % add all things that (may) need to be specified differently for each scan
scans = cell2struct( { '' , '' ,'MR MultiEcho FGRE', 11, 2, 1000;
                       '' , '' ,'MR IDEAL IQ', 11, 3, 1000; }, scanfields , 2);
            
%% Specify all scan sessions:
folder_cur{1} = 'D:\Theresa-Home\MRT_multicoil_Sep2020\MRT multicoil with asset\MEFGRE';
folder_cur{2} = 'D:\Theresa-Home\MRT_multicoil_Sep2020\MRT multicoil with asset\IDEAL IQ\Re+Im';

dataset = struct; 

%%
%pre-allocating 
ntimepoints = 53; %just for allocation
dataset.scans = repmat(scans, 1, ntimepoints);

for scanIdx = 1:size(dataset.scans,1)
    %get a list of all files and folders
    allFolders = dir(folder_cur{scanIdx});
    namesFolders = {allFolders.name};
    expression = dataset.scans(scanIdx,1).displayname;
    match = regexp(namesFolders, expression, 'match'); % searching for matches between expression in all folder names

    for k = 1:size(allFolders, 1)
       Flag(k) = ~isempty(match{k}); % only including names that match the search criteria
    end
    ResultScan = namesFolders(Flag);
    
    for k = 1:ntimepoints
        dataset.scans(scanIdx,k).foldername = deal(ResultScan{k}); % saving the foldernames in struct
        Dir_specific = fullfile(folder_cur{scanIdx}, ResultScan{k});
        files_inside = dir(fullfile(Dir_specific, '*dcm'));
        fileNames = {files_inside(1).name};    
        dataset.scans(scanIdx,k).filename = fileNames{end}; % saving the first DICOM file
    %     info = dicominfo(file);
    %     AcquisitionTime = info.AcquisitionTime;
    %     dicom_time_sec(k)= str2num(AcquisitionTime(1:2))*3600 + str2num(AcquisitionTime(3:4))*60 + str2num(AcquisitionTime(5:6));
    end
end 
%% Extract information needed for current scans:
allfn = cell(1, size(dataset.scans, 2)); % pre-allocating

for t = 1:numel(allfn)
    allfn{t} = fullfile( folder_cur {scanidx}, dataset.scans(scanidx, t).foldername, dataset.scans(scanidx, t).filename);
end

nechoes = dataset.scans(scanidx, 1).nechoes;
nchannels = 12;
dimensions = dataset.scans(scanidx,1).dimensions;

%tags needed later during post-processing
ScanArchive = 0;
LoopingStar = 0;