%%% performs MR thermometry on volunteer data
%%% works for multi-echo aquisitions from ScanArchives or DICOMS
%%% 
%%%   1. Loads data from datastructure
%%%   2. Performs complex image registration and transformation using Elastix
%%%   3. Calculates change in offresonance maps with WF-fitting tool (for multi-echo acquisitions)
%%%   4. Applies B0 drift correction using automatically selected internal body fat
%%%   5. Calculates the relative temperature change with PRFS
%%%   6. extracts MRT performance matrices
%%%
%%% version from 10-March-2023

% order of dimensions throughout
% dim_X = 1;
% dim_Y = 2;
% dim_slices = 3;
% dim_channels = 4;
% dim_echoes = 5;
% dim_timepoints = 6;


% set this
TP_ref = 1; %timepoint to reference to

%% Loading volunteer data information
for scan = 1:4 % to to all scans for brain data
   for subject = 1:10 % for all volunteers

subjidx = subject;
scanidx = scan;

datastructure_volunteers

%% Load data
if ScanArchive == 1
    [combinedImages, AqTime, AqTime_sec, TE, info_out, Dir] = import_coilCombine_multiEcho(allfn, nchannels, nechoes, ntimepoints, dimensions);
    X = info_out.xres;
    Y = info_out.yres;
    nslices = size(combinedImages, 3);
else %for DICOMS
    [combinedImages, TE, nslices, ntimepoints, AqTime, AqTime_sec, info_out, Dir] = import_ME_data(allfn, nechoes);
    X = info.matrix_size(1);
    Y = info.matrix_size(2);
end
  
 
%% Selecting folder with matching DICOM in order to extract information for T for image registration
% For different body parts

if ScanArchive == 1
        %for brain
    if scans(scanidx).Bodypart == "Brain"
        dicom_loading_folder = fullfile(curbase, 'MEFGRE_Brain_TP2');
    else %H&N in our case 
        dicom_loading_folder = fullfile(curbase, 'MEFGRE_H&N_TP2');
    end    
end 


%% Image registration using elastix

% when dicominfo is not available, load info from ME-fGRE acquisition to get information for transformation
    if ScanArchive == 1 %construct T from ScanArchive info
        pixel_spacing = [info_out.Resolution(1), info_out.Resolution(2), info_out.sliceThickness, 1];
        T = diag(pixel_spacing);
    else 
        T = affineTransformFromDicominfo(info);
    end 
    
    complex_trns = combinedImages; %pre-assigning size
    transform_parameters_all = cell(nslices);
    
if dimensions == 2 && ~exist(fullfile(Dir{ntimepoints},'complex_trns.mat'), 'file')   
%% for 2D - used for MRT calculations   
    pfiles = 'D:\Theresa-Home\elastix-5.0.1-win64\parameters_rigid.txt';
    for kk = 1:nslices % looping image registration & transformation over slices
        [complex_trans, transform_parameters, deformation] = complex_registration_transform(combinedImages(:,:,kk,:,:,:), T, Dir, pfiles);
        complex_trns(:,:,kk,:,:,2) = complex_trans(:,:,:,:,:,2);
    end
    
    %% saving complex_trns and transform parameters
    fileName = fullfile(Dir{ntimepoints}, 'complex_trns.mat');
    save(fileName, 'complex_trns');

    % check if it correctly alignes the images
elseif dimensions == 3 && ~exist(fullfile(Dir{ntimepoints},'complex_trns_3D.mat'), 'file')
%% for 3D - used to quantify motion in z direction (slice direction) 
    pfiles_3D = 'D:\Theresa-Home\elastix-5.0.1-win64\parameters_rigid3D.txt';    
    [complex_trns_3D, transform_parameters_3D, deformation3D] = complex_registration_transform(combinedImages(:,:,:,:,:,:), T, Dir, pfiles_3D);
    complex_trns_3D(:,:,:,:,:,1) = combinedImages(:,:,:,:,:,1);
    
    deformation_field = niftiread(deformation3D);
    maxSliceDefomation_mm = max(max(max(deformation_field(:,:,:,3))));
    
    szDefimg = size(deformation_field);
    deformationinImgCoord = reshape(reshape(deformation_field, [], szDefimg(end))*inv(T(1:3,1:3)), szDefimg ) ;
    deformation_slice = deformationinImgCoord(:,:,:,:,3);
    maxSliceDeformation_imgCoord = max(max(max(abs(deformation_slice))));
    fileName = fullfile(Dir{ntimepoints}, 'deformationinImgCoord.mat')    ;
    save (fileName, 'deformationinImgCoord')
    % saving complex_trns and transform parameters
    fileName = fullfile(Dir{ntimepoints}, 'complex_trns_3D.mat');
    save(fileName, 'complex_trns_3D');
     
else 
    if dimensions == 2
        tmp = load(fullfile(Dir{ntimepoints}, 'complex_trns.mat'));
        complex_trns = tmp.complex_trns;
    else % To use 3D image registration
        tmp = load(fullfile(Dir{ntimepoints}, 'complex_trns_3D.mat'));
        complex_trns_3D = tmp.complex_trns_3D;
        complex_trns = complex_trns_3D;
    end 
    
    tmp = load(fullfile(Dir{ntimepoints}, 'deformationinImgCoord.mat'));
    deformationinImgCoord = tmp.deformationinImgCoord;
    deformation_slice = deformationinImgCoord(:,:,:,:,3);
    maxSliceDeformation_imgCoord = max(max(max(abs(deformation_slice))));
end

% if maxSliceDeformation_imgCoord > 0.2
%     warning('The motion of volunteer %d (scan %d) is more than 20 percent of the slice thickness, therefore the MRT is no longer reliable', subjidx, scanidx)
% end 

%% calculating offresonance frequency change
if ~exist(fullfile(Dir{ntimepoints},'offres.mat'),'file')
    if nechoes <= 2
       %using both echoes by default. if wanting to use 1 echo, specify by varagin = 1 or 2, depending on the echo 
       offres_frequency = doubleEcho_offresChange(complex_trns, TP_ref, TE, info_out);
    elseif nechoes > 2
        [offres, rho_w, rho_f, fit_mask] = run_WF_fitting(complex_trns, TE, AqTime_sec, Dir);
        offres_frequency = zeros(size(offres));
        for ii = 1:ntimepoints
            offres_frequency(:,:,:,ii) = (offres(:,:,:,ii) - offres(:,:,:,TP_ref)); 
        end
        offres_frequency = permute(offres_frequency, [1,2,3,5,6,4]); %out: [x y slices echoes coils timepoints]
        rho_f = permute(rho_f, [1,2,3,5,6,4]);
        rho_w = permute(rho_w, [1,2,3,5,6,4]);
        fit_mask = permute(fit_mask, [1,2,3,5,6,4]);
    end
else
    tmp = load(fullfile(Dir{ntimepoints}, 'offres.mat'));
    offres = tmp.offres;
    tmp2 = load(fullfile(Dir{ntimepoints}, 'rho_f.mat'));
    rho_f = tmp2.rho_f;
    tmp3 = load(fullfile(Dir{ntimepoints}, 'rho_w.mat'));
    rho_w = tmp3.rho_w;
    tmp4 = load(fullfile(Dir{ntimepoints}, 'fit_mask.mat'));
    fit_mask = tmp4.fit_mask;

    offres_frequency = zeros(size(offres));
    for ii = 1:ntimepoints
        offres_frequency(:,:,:,ii) = (offres(:,:,:,ii) - offres(:,:,:,TP_ref)); 
    end
    offres_frequency = permute(offres_frequency, [1,2,3,5,6,4]); %out: [x y slices echoes coils timepoints]
    rho_f = permute(rho_f, [1,2,3,5,6,4]);
    rho_w = permute(rho_w, [1,2,3,5,6,4]);
    fit_mask =  permute(fit_mask, [1,2,3,5,6,4]);
end 

magnitude = abs(complex_trns);
%%
ME_loading_folder = dicom_loading_folder;

if ScanArchive == 1 && nechoes <= 2    
    data = load(fullfile(ME_loading_folder, 'rho_f.mat'));
    rho_f = data.rho_f; 
    data = load(fullfile(ME_loading_folder, 'rho_w.mat'));
    rho_w = data.rho_w;
    data = load(fullfile(ME_loading_folder, 'fit_mask.mat'));
    fit_mask = data.fit_mask;
    rho_f_new = zeros(X,Y,nslices,ntimepoints);
    rho_w_new = zeros(X,Y,nslices,ntimepoints); 
    fit_mask_new = zeros(X,Y,nslices,ntimepoints);
    
    for jj = 1:ntimepoints
        for kk = 1:nslices
            rho_f_tmp = rho_f(:,:,kk,jj);
            rho_f_new(:,:,kk,jj) = imresize(rho_f_tmp,[X,Y]);
            rho_w_tmp = rho_w(:,:,kk,jj);
            rho_w_new(:,:,kk,jj) = imresize(rho_w_tmp,[X,Y]);
            fit_mask_tmp = fit_mask(:,:,kk,jj);
            fit_mask_new(:,:,kk,jj) = imresize(fit_mask_tmp,[X,Y]);
        end 
    end
    rho_f = permute(rho_f_new, [1,2,3,5,6,4]);
    rho_w = permute(rho_w_new, [1,2,3,5,6,4]);
    fit_mask = permute(fit_mask_new, [1,2,3,5,6,4]);
end


%% Selecting the mask containing fatty voxels for B0 drift correction
% eliminating water-fat swapped voxels
blur_with = [4,4,0,0,0,0];
filtered_offres = gaussblur(offres_frequency, blur_with, double(fit_mask));

%getting rid of shading in the image
swapped_voxels = abs(offres_frequency - filtered_offres) > 200;
new_fit_mask = fit_mask;
new_fit_mask(swapped_voxels) = false;
filtered_offres2 = gaussblur(offres_frequency, blur_with, double(new_fit_mask));

% constraint including signal/noise threshold to avoid selecting non-fat voxels 
fatEcho = dataset(subjidx).scans(scanidx,1).fatEcho;
signalFractionThreshold = dataset(subjidx).scans(scanidx,1).signalFractionThreshold;

poorSignal_voxels = false(size(magnitude(:,:,:,:,fatEcho,:)));

if dimensions == 2
    for jj = 1:nslices
        for kk = 1:ntimepoints
            magnitude_tmp = abs(complex_trns(:,:,jj,:,fatEcho,kk));
            signal(jj,kk) = mean(maxk(reshape(magnitude_tmp,[],1),3)); %taking signal as strongest 3 max from mag image
            poorSignal_voxels(:,:,jj,:,:,kk) = magnitude_tmp < signalFractionThreshold .*signal(jj,kk);
        end
    end
else %for 3D (dimensions == 3)
    for kk = 1:ntimepoints
        magnitude_tmp = abs(complex_trns(:,:,:,:,fatEcho,kk));
        signal(kk) = max(max(max(magnitude_tmp))); %taking signal as max from mag image
        poorSignal_voxels(:,:,:,:,:,kk) = magnitude_tmp < signalFractionThreshold .*signal(kk);
    end
end 

%% B0 drift fat mask using rho_w and rho_f

water_threshold = dataset(subjidx).scans(scanidx,1).water_threshold;
noise_threshold = dataset(subjidx).scans(scanidx,1).noise_threshold;

% General threshold for fat selection using original rho_w and rho_f
maskFat = zeros(size(rho_f));
for kk = 1:nslices % going per slice, because intensity varies with region
    maskFat(:,:,kk,:,:,:) = (rho_f(:,:,kk,:,:,:) > rho_w(:,:,kk,:,:,:).*water_threshold) & (rho_f(:,:,kk,:,:,:) > noise_threshold.*mean(maxk(reshape(rho_f(:,:,kk,:,:,:),[],1),3))); %to eliminate noisy regions
    %added 3 highest values as signal to get rid of large outlier bias
end

% applying signal/noise threshold defined above
maskF_lessvoxels = maskF;
maskF_lessvoxels(noFat_voxels) = false;

%% combining 3 masks from above
maskF = maskFat & new_fit_mask; %excluding the fat/water swapped voxels

% applying signal/noise threshold defined above
maskF_lessvoxels = maskF;
maskF_lessvoxels(poorSignal_voxels) = false; 

if nechoes == 2
    maskF_final = maskF_lessvoxels;
else  
    % get rid of falsely slected vessels using FSL - ONLY BRAIN - do not use for DEGRE
    iMag_abs = sum(abs(combinedImages(:,:,:,1,:,1)),5); % summing all the echoes for better signal
    matrix_size = size(iMag_abs);
    voxel_size = [1.5 1.5 1.5]; %mm
    fractional_threshold = 0.4; % 0.5 default    
    only_brain_mask = BET(iMag_abs,matrix_size,voxel_size,fractional_threshold);
    Not_mask = not(only_brain_mask);  
    Not_mask = repmat(Not_mask, [1 1 1 1 1 2]);
    % adding BET mask to fat mask
    maskF_final = logical(Not_mask.*maskF_lessvoxels);
end 

%% Fat correction based on Dixon method
threshold_residual_outliers = dataset(subjidx).scans(scanidx,1).threshold_residual_outliers;

x = (1:size(maskF,1))-size(maskF,1)/2;
y = (1:size(maskF,2))-size(maskF,2)/2;

% if image3D
if dimensions == 3
    z = (1:size(maskF,3))-size(maskF,3)/2;
    [X,Y,Z] = meshgrid(x,y,z);
else
    [X,Y] = meshgrid(x,y);
end

% for linear correction
shimfields = ones(length(X(:)),3);
shimfields(:,2) = X(:); % linear terms
shimfields(:,3) = Y(:);
if dimensions == 3
    shimfields(:,4) = Z(:);
    % for 2nd order correction
    shimfields(:,5:10) = [X(:).^2, X(:).*Y(:), Y(:).^2, X(:).*Z(:), Y(:).*Z(:), Z(:).^2];
end

%pre-allocating variables
dT_bias = zeros(size(offres_frequency));
residual = cell(nslices, ntimepoints);
total_outliers = zeros(nslices, ntimepoints);
rms_residual_check = cell(nslices, ntimepoints);

if dimensions == 2
    for jj = 1:ntimepoints
        for kk = 1:nslices  % for 3D do all slices at once. 
            offres_temp = offres_frequency(:,:,kk,:,:,jj);
            mask_temp = maskF_final(:,:,kk,:,:,jj);
            N_outliers = inf;
            while N_outliers > 0           
                X_mat = shimfields(mask_temp(:),:);   
                w = X_mat\offres_temp(mask_temp); % fit bias fields within fat pixels
                residual{kk,jj} = offres_temp(mask_temp)-(X_mat*w); % data - prediction
                % residual should not be very large, use to exclude outliers
                keep_sample = abs(residual{kk,jj}) < threshold_residual_outliers; % these are included
                mask_temp(mask_temp) = keep_sample;
                N_outliers = sum(~keep_sample); 
                total_outliers(kk,jj) = N_outliers;
            end

            maskF_final(:,:,kk,:,:,jj) = mask_temp;
            rms_residual_check{kk,jj} = rms(residual{kk,jj}); %want to check whether this is >> noise level of B0 map        
            dT_bias(:,:,kk,1,1,jj) = reshape(shimfields*w, size(mask_temp));

        end
    end
else % for 3D: doing all slices at once 
    for jj = 1:ntimepoints
        offres_temp = offres_frequency(:,:,:,:,:,jj);
        mask_temp = maskF_final(:,:,:,:,:,jj);
        N_outliers = inf;
        while N_outliers > 0           
            X_mat = shimfields(mask_temp(:),:);   
            w = X_mat\offres_temp(mask_temp); % fit bias fields within fat pixels
            residual{jj} = offres_temp(mask_temp)-(X_mat*w); % data - prediction
            % residual should not be very large, use to exclude outliers
            keep_sample = abs(residual{jj}) < threshold_residual_outliers; % these are included
            mask_temp(mask_temp) = keep_sample;
            N_outliers = sum(~keep_sample); 
            total_outliers(jj) = N_outliers;
        end
        maskF_final(:,:,:,:,:,jj) = mask_temp;

        rms_residual_check{jj} = rms(residual{jj}); %want to check whether this is >> noise level of B0 map        
        dT_bias(:,:,:,1,1,jj) = reshape(shimfields*w, size(mask_temp));
    end
end 

offres_frequency_corr = (offres_frequency - dT_bias);

%% Calculating temperature changes with PRFS
% constants for MRT
B0 = 1.5; % Main magnetic field (T)
gamma = 267.513e6; % gyromagnetic ratio (rad.s-1.T-1)
alpha = -0.01e-6; % PRFS constant (C-1) between -0.009 and -0.01 ppm/�C
constant_fit = -gamma*alpha*B0;

MRT_corr = offres_frequency_corr./constant_fit; % �C


%% Thresholding with rho_f and rho_w to eliminate unreliable MRT regions

enoughWater = rho_f < 0.2.*rho_w; % up to 20% of fat signal
enoughSignal = ~poorSignal_voxels;
mask_all = logical(enoughWater.*enoughSignal);

MRT_reliable = MRT_corr.*mask_all;


%% selecting ROI
tmp = magnitude(:,:,:,:,1,2); %[x y slices channels contrasts tp]
% need dim_constrasts = 1, dim_ROI = 2&3, dim_slices = 4 ;
images = permute(tmp, [4 1 2 3]); 
name_mask_file = 'whole_brain_volunteer.mat';
maxNumROIslices = -1; 
destfolder = {'D:\Theresa-Home\volunteer_ROIs', name_mask_file};
maskpurpose = 'ROI for volunteer MR thermometry performance';
[M, Mx, My, ~, masksource] = drawROI(images, maxNumROIslices, destfolder, maskpurpose, 'ROItype', 2);
M = M(:,:,1);
ROI_voxels = sum(sum(M == 1)); %size of mask. Is this valuable?
%%
ROI_mean = zeros(nslices, ntimepoints);
ROI_std = zeros(nslices, ntimepoints);
for jj = 1:ntimepoints
    for kk = 1:nslices
        tmp = MRT_reliable(:,:,kk,:,:,jj);
        ROI_mean(kk,jj) = mean(tmp(M));
        ROI_std(kk,jj) = std(tmp(M),1);
    end 
end

%% creating and saving outcome .mat file containing log of parameters and more 
outcome = struct;
outcome.inputData_dir = Dir;
outcome.mean = ROI_mean;
outcome.std = ROI_std;
outcome.ROImask = destfolder;
outcome.ROIsize = ROI_voxels;
outcome.maskF_final = maskF_final;
outcome.mrt_constants = {'B0', B0, 'gamma', gamma, 'alpha', alpha};
outcome.dT_bias = dT_bias;
if nechoes == 2
    outcome.additional_files_fatmap_source = ME_loading_folder;
end
outcome.unreliable_slices = position_bad_slice;
outcome.deformationinImgCoord = deformationinImgCoord;
outcome.maxSliceDeformation_imgCoord = maxSliceDeformation_imgCoord;

%%
results_basefolder = 'D:\Theresa-Home\volunteer_results';
results_path = fullfile(results_basefolder, dataset(subjidx).basefolder);

if ~exist(results_path, 'dir')
    mkdir(results_path)
end 

extension = '.mat';
results_name = [dataset(subjidx).scans(scanidx,1).displayname, extension];
results = fullfile(results_path, results_name);
save(results,'outcome')

   end
end 

