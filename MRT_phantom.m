%%% performs MR thermometry on phantom data
%%% works with aquisitions from ScanArchives or dicoms
%%% 
%%%   1. Loads data from ScanArchives or dicoms
%%%   2. Calculates change in offresonance 
%%%   3. Applies B0 drift correction by selecting external fat tubes
%%%   (automatically OR manually)
%%%   4. Calculates the relative temperature change with PRFS
%%%   5. Imports "true" temperature data from BSD probes
%%%   6. calculates MRT performance and exports those in Excel sheet

%%% NB. 3D-MEFGRE is noted sometimes as IDEAL IQ, which is the GE specific name for it

%  version from 19-05-2022 
% 14-3-2023: T. Feddersen modified, working with thresholds from datastruct

%order of dimensions throughout
% dim_X = 1;
% dim_Y = 2;
% dim_slices = 3;
% dim_channels = 4;
% dim_echoes = 5;
% dim_timepoints = 6;


% set this
TP_ref = 1; %timepoint to reference to
% image3D not used yet
image3D = 0; %whether to evaluate as 2D or 3D

selected_slice = 3; %which slice (if only one) should be evaluated


%% for multi channel data sep 2020
%   scanidx: 1-2 ordered by time of acquisition ie always:
%       1 = DEGRE TR 620 , 3 slices // MEFGRE 
%       2 = DEGRE TR 200 , 3 slices // IDEAL IQ, 12 slices

scanidx = 1;
% datastructure_phantom_MRT_sep2020
datastructure_phantom_MRT_sep2020_dicoms
% 
nslices = 3; %for ME-FGRE
% nslices = 12; %for IDEAL IQ


%% Load data 
if ScanArchive == 1
    %for ScanArchives
    [combinedImages, AqTime, AqTime_sec, TE, info_out, Dir] = import_coilCombine_multiEcho(allfn, nchannels, nechoes, ntimepoints, dimensions);
else 
    %for DICOMS
    [combinedImages, AqTime, AqTime_sec, TE, info_out, Dir] = import_DICOMs(allfn, nslices, nechoes, ntimepoints, dimensions);

end 


%% B0 drift correction 
% fat mask selection for phantom
% using only outer fat tubes
figure, imagesc(magnitude(:,:,selected_slice,1,1,1))

% manually select areas NOT to be used as reference ie the middle part
h = drawcircle;
mask_nofat = createMask(h);
mask_ref = ~mask_nofat;

fatmap = magnitude(:,:,:,1,1,:).*mask_ref; 
fatmap_threshold = dataset.scans(scanidx,1).fatmap_threshold;
maskF = fatmap > fatmap_threshold;
maskF = repmat(maskF, [1,1,nslices,1,1,ntimepoints]);



%% used for calculation of reduced termperature range phantom experiments Sep 2020
% ntimepoints = 5;
% TP_ref = 5;
% TP_ref = 43;


%% calculating offresonance frequency change
if ~exist(fullfile(Dir{ntimepoints}, 'offres.mat'),'file')
    if nechoes <= 2
        %using both echoes by default. if wanting to use 1 echo, specify by varagin = 1 or 2, depending on the echo 
        offres_frequency = doubleEcho_offresChange(combinedImages, TP_ref, TE, info_out);
         
    elseif nechoes > 2
        [offres, rho_w, rho_f, fit_mask] = run_WF_fitting(combinedImages, TE, AqTime_sec, Dir);
        offres_frequency = zeros(size(offres));
        for ii = 1:ntimepoints
            offres_frequency(:,:,:,ii) = (offres(:,:,:,ii) - offres(:,:,:,TP_ref)); 
        end
        offres_frequency = permute(offres_frequency, [1,2,3,5,6,4]); %out: [x y slices echoes coils timepoints]
    end
else
    tmp = load(fullfile(Dir{ntimepoints}, 'offres.mat'));
    offres = tmp.offres;

    offres_frequency = zeros(size(offres));
    for ii = 1:ntimepoints
        offres_frequency(:,:,:,ii) = (offres(:,:,:,ii) - offres(:,:,:,TP_ref)); 
    end
    offres_frequency = permute(offres_frequency, [1,2,3,5,6,4]); %out: [x y slices echoes coils timepoints]
end 


%% fat correction based on DIXON
nslices = size(offres_frequency, 3);

x = (1:size(maskF,1))-size(maskF,1)/2;
y = (1:size(maskF,2))-size(maskF,2)/2;

if image3D == 1
    z = (1:size(maskF,3))-size(maskF,3)/2;
    [X,Y,Z] = meshgrid(x,y,z);
else % if image3D == 0 , will do 2D B0 correction
    [X,Y] = meshgrid(x,y);
end

shimfields = ones(length(X(:)),3);
shimfields(:,2) = X(:); % linear terms
shimfields(:,3) = Y(:);
if image3D == 1
    shimfields(:,4) = Z(:);
    % for 2nd order correction
    shimfields(:,5:10) = [X(:).^2, X(:).*Y(:), Y(:).^2, X(:).*Z(:), Y(:).*Z(:), Z(:).^2];
end

dT_bias = zeros(size(offres_frequency));

if image3D == 0
    for jj = 1:ntimepoints
        for kk = 1:nslices
            offres_temp = offres_frequency(:,:,kk,:,:,jj);
            mask_temp = maskF(:,:,kk,:,:,jj);
            X_mat = shimfields(mask_temp(:),:);   
            w = X_mat\offres_temp(mask_temp);  % fit bias fields within fat pixels
            test{kk,jj} = w;
            dT_bias(:,:,kk,1,1,jj) = reshape(shimfields*w, size(mask_temp));
        end
    end
else % for 3D: doing all slices at once 
    for jj = 1:ntimepoints
         offres_temp = offres_frequency(:,:,:,:,:,jj);
         mask_temp = maskF(:,:,:,:,:,jj);
         X_mat = shimfields(mask_temp(:),:);   
         w = X_mat\offres_temp(mask_temp);  % fit bias fields within fat pixels
         test{kk,jj} = w;
         dT_bias(:,:,:,1,1,jj) = reshape(shimfields*w, size(mask_temp));
    end
end 


offres_frequency_corr = (offres_frequency - dT_bias);


%% Calculating temperature changes with PRFS
% constants for MRT
B0 = 1.5; % Main magnetic field (T)
gamma = 267.513e6; % gyromagnetic ratio (rad.s-1.T-1)
alpha = -0.01e-6; % PRFS constant (C-1) between -0.009 and -0.01 ppm/�C
constant_fit = -gamma*alpha*B0; %without the -ve for ME sequences

MRT_corr = offres_frequency_corr./constant_fit; % �C

%% Thresholding with rho_f and rho_w to eliminate unreliable MRT regions

enoughWater = rho_f < 0.2.*rho_w; % up to 20% of fat signal

enoughSignal = magnitude(:,:,:,:,1,:) > 250;

mask_all = logical(enoughWater.*enoughSignal); 

MRT_reliable = MRT_corr.*mask_all;

%% Selecting ROI
cd 'D:\Theresa-Home\phantom_ROIs'
tmp = magnitude(:,:,selected_slice,1,1,1); %[x y slices(z) channels contrasts(echoes) TPs]
% need dim_constrasts = 1, dim_ROI = 2&3, dim_slices = 4 ;
images = permute(tmp, [4 1 2 3]); 
name_mask_file = 'experiment phantom';
maxNumROIslices = -1; 
destfolder = {'D:\Theresa-Home\phantom_ROIs', name_mask_file};
maskpurpose = 'ROI for phantom MRT';
[M, Mx, My, ~, masksource] = drawROI(images, maxNumROIslices, destfolder, maskpurpose, 'ROItype', 2);

ROI_voxels = sum(sum(M == 1)); %size of mask

nROIs = 1;




%% performance calculatons
MRT_corr = MRT_reliable; %to eliminate the part where FF > 20%

ROI_mean = zeros(ntimepoints, nslices, nROIs);
ROI_std = zeros(ntimepoints, nslices, nROIs);
for ii = 1:nROIs
    for jj = 1:ntimepoints
        for kk = 1:nslices
            tmp = MRT_corr(:,:,kk,:,:,jj);
            ROI_mean(jj,kk,ii) = mean(tmp(M(:,:,ii)));
            ROI_std(jj,kk,ii) = std(tmp(M(:,:,ii)),1); % w=1 to correspond with formula defined in paper SD^2=1/N SUM(i=1:N)(|Ej-mean(E)|^2)
        end 
    end
end

%% BSD

[file_name, folder_name] = uigetfile('*.*','Select the BSD data','MultiSelect','on');
BSD_data = strcat(folder_name, file_name);
BSD(:,:) = xlsread(BSD_data); 


BSD_time_secs = BSD(3:end,1); %first 2 entries are not wanted

% taking only water probe for multi channel experiment
T_BSD = BSD(3:end,5);  %probe 4 was used

% clear BSD

%% To match time of BSD and MRI MULTI channel phantom experiment 14-09-2020
%BSD started recording 10:00:58
%time match showed that the MRI is 03:52 ahead
%actual difference in time = BSD starting time - difference internal clock BSD/MRI - dicom starting time
% 
% difference_time_sec = (10*60*60+58) + (3*60+52) - AqTime_sec(TP_ref);
% BSD_time_sec_true = BSD_time_secs + difference_time_sec;
% BSD_time_mins_true = BSD_time_sec_true/60;


%% To match time of BSD and MRI MULTI channel phantom experiment 01-12-2020
%BSD started recording 10:42:00
%time match showed that the MRI is 55:27 BEHIND
%actual difference in time = BSD starting time - difference internal clock BSD/MRI - dicom starting time

difference_time_sec = (10*60*60+42*60) - (55*60+27) - AqTime_sec(TP_ref);
BSD_time_sec_true = BSD_time_secs + difference_time_sec;
BSD_time_mins_true = BSD_time_sec_true/60;



%% Extracting acuracy, precision and bias of MRT measurements
% To find closest matching value of the BSD temperature for each timepoint
dt_MRI_secs = (AqTime_sec - AqTime_sec(TP_ref)) ;
dt_MRI_mins = (AqTime_sec - AqTime_sec(TP_ref)) /60;


bestDifference = size(ntimepoints);
closestIndex = size(ntimepoints);
BSD_timepoint_secs = size(ntimepoints);
BSD_T_timepoints = size(ntimepoints);

for m = 1:ntimepoints
    [bestDifference(m), closestIndex(m)] = min(abs(BSD_time_sec_true - dt_MRI_secs(m)));
    BSD_timepoint_secs(m) = BSD_time_sec_true(closestIndex(m));
    BSD_T_timepoints(m) = (T_BSD(closestIndex(m))); 
end

% mean_ROI_abs = ROI_mean + BSD_T_timepoints(TP_ref);
mean_ROI_abs = ROI_mean + T_BSD(closestIndex(TP_ref));

dT_probe = T_BSD - T_BSD(closestIndex(1));

%% calculate performance values

accuracy_ROI = zeros(1,nslices);
temp_precision_ROI = zeros(1,nslices);
spat_precision_ROI = zeros(1,nslices);
bias_ROI = zeros(1,nslices);

for k = 1:nslices
    %bias == systematic error: the mean difference between probe measurements and ROI
    bias_ROI(k) = mean(mean_ROI_abs(:,k) - BSD_T_timepoints');
    
    % spatial precision: mean of the spatial variability in each ROI
    spat_precision_ROI(k) = mean(ROI_std(:,k));
    
    % temporal precision: variability across ROI different timepoints
    temp_precision_ROI(k) = std(ROI_std(:,k));
    
    % accuracy: mean of the absolute deviation from ground truth
    accuracy_ROI(k) = mean(abs(mean_ROI_abs(:,k) - BSD_T_timepoints')); %deg C
end



%% creating and saving outcome .mat file containing log of parameters and more 
outcome = struct;
outcome.inputData_dir = Dir;
outcome.mean = ROI_mean;
outcome.std = ROI_std;
outcome.ROImask = destfolder;
outcome.maskF = maskF;
outcome.mrt_constants = {'B0', B0, 'gamma', gamma, 'alpha', alpha};
outcome.dT_bias = dT_bias;
outcome.closestIndex = closestIndex;
outcome.accuracy = accuracy_ROI;
outcome.spatial_precision = spat_precision_ROI;
outcome.temporal_precision = temp_precision_ROI;
outcome.bias = bias_ROI;

%%
results_basefolder = 'D:\Theresa-Home\phantom_results';
results_path = fullfile(results_basefolder, 'multi_channel_phantom_2020');
if ~exist(results_path, 'dir')
    mkdir(results_path)
end 

extension = '.mat';
results_name = [dataset.scans(scanidx,1).displayname, extension];
results = fullfile(results_path, results_name);
save(results,'outcome')

