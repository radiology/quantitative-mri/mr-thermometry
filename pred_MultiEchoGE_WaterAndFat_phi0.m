function [img, dimg ] = pred_MultiEchoGE_WaterAndFat_phi0( theta2, TE, SFat)
%[img, dimg ] = pred_MultiEchoGE_WaterAndFat( theta, TE, FatPeakRatio, FatFreq )
%
% Predicts the signal intensities of a multi echo gradient echo image
% acquired with multiple coils. Allows estimation of (weighted) PD, R2star,
% B0, and complex coil sensitivity profiles (by using this function with fit_MRI).
% 
% Signal model:
%  S(i) = (rho_w + rho_f * SFat) * exp(-TE(i) * (R2star + 1i* offresefreq) - 1i*phi_0);
% i= number of echoes.
% j= number of fat peaks.
%
% img = [real(S); imag(S)]
% Construct  SFat  with SFat = makeSFat(TE, FatPeakRatio, FatFreq );
%    or extract from scanned (reference) fat at zero offresonance w.r.t. water. 
%
% INPUTS:
%  theta : npar x nvoxels  : input parameters
%        [rho_w     :   PD of water
%         rho_f     :   PD of fat
%         R2star:  relaxation ratio due to B0 inhomogeneities for water and fat in 1/unit-time.
%         offresfreq: off-resonanc frequency of water in rad/unit-time
%         phi_0: phase at TE = 0 in rad
%           ]
%  TE  : nTE element column vector with echo times in unit-time.
%  FatPeakRatio_j: relative normalized amplitude of a the jth fat peak 
%  FatFreq_j : off resonance frequencency of the jth fat peak relative to water in unit rad/unit-time. 
%
% OUTPUTS:
%  img  : (2*numel(TE))  x nvoxels real valued images :
%          [ real_echo1 .....till echo n
%            imag_echo1......till echo n]
%  dimg : (...) x nvoxels x size(theta,1)   
%         derivative of img w.r.t. par (i.e. exluding the parts from fields)
%
% Created by Dirk Poot, Erasmus MC, 15-7-2014
%  Based on pred_MultiechoGE


% Split parameters:
rho_w = theta2( 1 , : );
rho_f = theta2( 2 , : );
R2star = theta2(3 , : );
offresfreq = theta2(4 , : );
phi_0 = theta2(5 , : );


DsDrho_w = exp( bsxfun(@plus, -TE * (R2star + 1i*offresfreq), - 1i*phi_0));
S = bsxfun(@plus, rho_w , SFat * rho_f ) .* DsDrho_w;   

img = [real(S);imag(S)];

if nargout>1
%     derivative of signal wrt parameters
%     dimg = zeros([size(img) size(theta,1)]);
    
    DsDrho_f = bsxfun(@times, DsDrho_w , SFat);
    DsDR2star = bsxfun(@times, bsxfun(@plus, rho_w , (bsxfun(@times, rho_f, (bsxfun(@rdivide, DsDrho_f, DsDrho_w))))), bsxfun(@times, -TE, DsDrho_w));
    DsDoffresfreq = 1i * DsDR2star;
    DsDphi_0 = bsxfun(@rdivide, DsDoffresfreq, TE);
        
    dS = cat(3, DsDrho_w, DsDrho_f, DsDR2star, DsDoffresfreq, DsDphi_0);
    
    dimg = [real(dS);imag(dS)];
end;