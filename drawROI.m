function [M, Mx, My, sliceno, masksource] = drawROI(images, maxNumROIslices, destfolder , maskpurpose, varargin)
% [M, Mx, My, sliceno, masksource] = drawROI( images, maxNumROIslices, folder, maskpurpose [, option1, value1, ...] )
%
% Asks the user to draw an ROI or load from disk.
%
% INPUTS:
%   images : [contrasts  spatial_size] 
%         a set of images as can be input for fit_MRI
%         so first dimension is different contrasts
%         and ROI drawn on 2nd and 3rd dimension.
%         If the user closes this figure a dialog box opens with the
%         request to specify a file from which the mask can be loaded.
%   maxNumROIslices: -1 : the ROI is copied to all slices (4th dimension)
%                   0 : only loaded from file.
%                  >0 : The number of slices that you may draw (typically 1 or inf)
%   folder : Specifies the folder in which the dialog opens (when the intial
%         figure containing the images is closed by the user)
%         NOTE: If this is a file the mask is loaded from the file and the user is
%         not asked. 
%         Alternatively: a 2 element cell array with first element 'folder'
%         and second element a the name of a file where the mask is stored to.
%   maskpurpose: String that explains the user what this mask is for.
%         default = 'All images on which the fit will be performed.';  
%
%   option value pairs, or scalar structure with fields:
%     ROItype : 1  : (default) draw polynomial
%               2  : draw ellipse
%               3  : draw rectangle
%               4  : freehand drawing
%
% OUTPUTS:
%   M  : binary images of size spatial_size 
%        true is selected inside the mask. 
%   Mx : cell array with lists of x coordinates of the drawn ROI. 
%   My : cell array with lists of x coordinates of the drawn ROI. 
%   sliceno : vector with slice numbers
%   masksource : string with description where the mask came from. (filename, or '-user drawn-' or '-none-')
% 
% Created by Dirk Poot, TUDelft, 27-3-2014
% 13-11-2019, D.Poot, Erasmus MC : updated help; few weeks ago added option-value pairs with ROItype. 

%  OBSOLETE SECOND ARGUMENT: copy2slices : if true the ROI is copied to all slices (4th dimension)
%         otherwise the mask is only nonzero in the slice in which the ROI
%         is drawn

% edited by Theresa Feddersen 6-9-2021

opt.ROItype = 1; 
opt.ROIdims = [2 3]; % TODO: use these dimensions everywhere; currently (19-9-2019) its not. 
if nargin<3
    destfolder = {[],[]};
end;
if nargin>=5
    opt = parse_defaults_optionvaluepairs( opt, varargin{:} );
end;
if ~iscell(destfolder)
    destfolder = {destfolder , [] };
end;
if ~(exist(destfolder{1},'file')==2) && maxNumROIslices~=0 % skip figure if a mask file is provided.

    if nargin<4 || isempty(maskpurpose )
        maskpurpose = 'All images on which the fit will be performed.'; 
    end;
    figno = figure; 

    descr = [maskpurpose ' Please draw the ROI in one slice (in dimension 2 - 3).'];
    maxDrawROISlices = min(2*size(images,4),abs(maxNumROIslices));
    if maxNumROIslices==-1
        descr = [descr ' This ROI will be copied to all other slices.'];
    elseif maxNumROIslices >=1
        descr = [descr ' You may draw up to ' num2str( maxNumROIslices) ' ROI''s, which will be combined into a single mask. You may close the figure when you don''t need do add further ROI''s.'];
    else
        error('invalid maxNumROIslices provided');
    end;
    newline = sprintf('\n');
    descr  = [descr newline 'Alternatively you can close this figure before drawing any ROI and a file browser allows you to select a mat file from which the variable mask is loaded as mask.'];
    figno = imagebrowse(images,[],'description',descr);
    ud = get(figno,'userdata');
    m = ( ud.subfigdims(:,1)==opt.ROIdims(1) & ud.subfigdims(:,2)==opt.ROIdims(2) );
    axid = ud.subfigs( m );
    ud.ROIs_subplotidx = cell(1,size(ud.subfigdims,1));
    ud.ROIs_subplot =  cell(1,size(ud.subfigdims,1));
    Mslice = cell(1, maxDrawROISlices);
    Mx = Mslice;
    My = Mslice;
    sliceno = zeros(1, maxDrawROISlices);
    numslicesfilled = maxDrawROISlices;
    set(figno,'userdata',ud);
    sliceRoiidx = 1;
    while sliceRoiidx <= maxDrawROISlices
        set(figno,'CurrentAxes',axid) ; %,'name','IR - Images');
        switch opt.ROItype
            case 1
%                 [Mslice{sliceRoiidx}, Mx{sliceRoiidx}, My{sliceRoiidx}] = roipoly;
                ROIhandle = impoly;
            case 2
                ROIhandle = imellipse;
            case 3
                ROIhandle = imrect;
            case 4
                ROIhandle = imfreehand;
            otherwise
                error('Unsuported ROItype specified.');
        end;
        Mxy = wait(ROIhandle);
        delete( ROIhandle );
        if ~ishandle(figno)
            numslicesfilled = sliceRoiidx-1;
            break;
        end;
        if isempty( Mxy) 
            continue;
        end;
        Mx{sliceRoiidx} = Mxy(:,1);
        My{sliceRoiidx} = Mxy(:,2);
        Mslice{sliceRoiidx} = poly2mask( Mx{sliceRoiidx}, My{sliceRoiidx}, size( images, opt.ROIdims(1)) , size(images, opt.ROIdims(2) ) );

        if isempty(Mslice{sliceRoiidx})
            continue; % user canceled drawing of polygon. Just let him try again.   
        end;
        ud = get(figno,'userdata');
        if any(ud.tileDims==4) 
            sliceno(sliceRoiidx) = round( get(ud.sliders(ud.tileDims==4),'value')) ;
        else % single slice dataset. 
            sliceno(sliceRoiidx) = 1;
        end;
        curdims = ud.subfigdims(m,:);
        curtiledims = find( ~ismember( ud.tileDims, curdims));
        ud.ROIs{end+1} = nan(numel(Mx{sliceRoiidx}),ndims(ud.IMG)); % make ROIs non empty
        ud.ROIs{end}(:,curdims) = [My{sliceRoiidx}(:) Mx{sliceRoiidx}(:)];
        ud.ROIs_subplotidx{m}(end+1,:) = nan(size(curtiledims));
        ud.ROIs_subplotidx{m}(end, curtiledims==4 ) = sliceno(sliceRoiidx);
        ud.ROIs_subplot{m}(end+1) = numel(ud.ROIs);
        set(figno,'userdata',ud);
        ud.redrawsubplots( find( m ) ); %#ok<FNDSB> redrawsubplots is a function expecting a list of integers
        drawnow;
        sliceRoiidx =  sliceRoiidx+1;
    end;
    if ishandle(figno)
        close(figno);
    end;
else
    Mslice = {[]};
    numslicesfilled = 0 ;
end;

if numslicesfilled==0%~ishandle(figno)
    %% load from file:
    % load from disk:
    if exist(destfolder{1},'file')==2
        masksource = destfolder{1};
    else
        [FileName,PathName,FilterIndex] = uigetfile({'*.mat', 'MATLAB mat files; they should contain a mask';
                                             '*.nii', 'nifti images with a mask';
                                             '*.nii.gz', 'nifti images with a mask'}, 'Load a mask file', fullfile(destfolder{1}));
        if FileName==0
            masksource = [];
        else
            masksource = [PathName FileName];
        end;
    end;
    if exist(masksource ,'file')==2
        [folder, filename, ext ] = fileparts( masksource ); %#ok<ASGLU>
        if strcmpi(ext,'.nii') || strcmpi(ext,'.gz')
            tmp = load_niiT( masksource );
            loadmask = struct; 
            loadmask.mask = tmp.img;
        else
            loadmask = load(masksource,'mask','mask_x','mask_y','mask_slicenumbers');
        end;
        szimg = size(images);
        xy_image = szimg(2:min(numel(szimg),3)); %TFN
        szmask = size(loadmask.mask);
        if ~isequal(szimg(2:min(numel(szimg),3)) , szmask)
%             error('The mask loaded from the file should have the same size as the current image'); %TFN
            tmp = loadmask.mask; %TFN
            M = imresize(tmp,xy_image); %TFN
        else %TFN
            M = loadmask.mask; %TFN
        end; %TFN
%         M = loadmask.mask; %TFN
        if isfield(loadmask,'mask_x') && isfield(loadmask,'mask_y')
            Mx = loadmask.mask_x;
            My = loadmask.mask_y;
        else
            Mx = nan;
            My = nan;
        end;
        if isfield(loadmask,'mask_slicenumbers')
            sliceno = loadmask.mask_slicenumbers;
        else
            szmask(end+1)=1; % make sure a 3rd element exists
            sliceno = find(any(reshape(M,[prod(szmask(1:2)) szmask(3)]),1));
        end;
    else
        % no valid file selected:
        M = [];
        Mx = [];
        My = [];
        sliceno = [];
        masksource = '-none-';
    end;
else
    if maxNumROIslices==-1 || size(images,4)==1
        % copy to all slices:
        sliceno = 1:size(images,4);
        M = repmat( Mslice{1},[1 1 size(images,4) ]);
    else
        % set in the specific slice on which it was drawn:
        M = false( [size(images,2), size(images,3), size(images,4)]);
        for k = 1 : numslicesfilled
            M( : ,: , sliceno(k) ) = M( : ,: , sliceno(k) ) | Mslice{k};
        end;
%             sliceno = round( get(ud.sliders(ud.tileDims==4),'value')) ;
%         M(:)=false;M(:,:,sliceno)=Mslice;
    end;
    
    masksource = '-user drawn-';
end;

if numel(destfolder) >= 2 && ~isempty( destfolder{2} ) && ~isempty(M) && ~isequal(masksource, destfolder{2} )
    %% save mask:
    dosave = true; 
    save_fn = string(destfolder{2});
%     if exist(save_fn,'file')
% %         YN = input(['The file "' save_fn '"\n already exists, do you want to overwrite? (Y/N) :'],'s');
%         YN = input(['The file "' save_fn '" already exists, do you want to overwrite? (Y/N) :'],'s');
%         if ~isequal(lower(YN),'y') && exist(save_fn,'file')
%             disp(['You typed "' YN '", which is not ''y'' or ''Y'', interpretting as no ']);
%             dosave = false; 
%             warning('the mask is not saved');
%             %error('cant continue as I should not overwrite the result file.');
%         end;
%     end;
    if dosave
        S = struct;
        S.mask = M;
        S.mask_x = Mx;
        S.mask_y = My;
        S.mask_slicenumbers = sliceno;
        S.masksource = masksource;
        save(save_fn, '-struct', 'S' );
    end;
end;