function [combinedImages, AqTime, AqTime_sec, TE, info_out, DIR] = import_coilCombine_multiEcho(allfn, nb_coils, nb_echoes, nb_timepoints, dimensions)
%  [combinedImages, AqTime, AqTime_sec, TE, info_out, Dir] = import_coilCombine_multiEcho(allfn, nchannels, nechoes, ntimepoints, dimensions)
%
% Imports ScanArchive files specified by allfn for echoes >=2 and
% performes a coil sensitivity combination if needed. Reconstructs k-space
% and returns complex valued image data amongst other information
% 
% 
% INPUT:  allfn = cell containing ScanArchive filenames of data to be loaded
%         nb_coils = number of coils, if >1, will be coil combined
%         nb_timepoints = number of timepoints 
%         nb_echoes = number of echoes
%         dimensions = 2 for 2D or 3 for 3D(double) 
% 
% OUTPUT:     combinedImages = complex values format: [Ny Nx Nz channels echoes timepoints]
%             AqTime = acquisition time clock format: HH:mm:ss,
%             AqTime_sec = acquisition time in seconds 
%             TE = echo times for all echoes (s) 
%             info_out = ScanArchive info about scan
%             DIR = working directory, smallest level possible
%
% Theresa Feddersen, edited 18-01-2022

if ~exist('nb_timepoints', 'var')
    nb_timepoints = size(allfn,2);
end 

DIR{nb_timepoints} = [];
AqTime{nb_timepoints} = [];
AqTime_sec = zeros(nb_timepoints,1);
% combinedImages = zeros(128, 128, nb_slices, 1, nb_echoes, nb_timepoints); 

for k = 1:nb_timepoints
    
    if isempty(allfn{k}) 
        answer = input('There is no input data location provided. Do you want to continue, Y/N [Y]:','s');
        if answer == 'N'
            break 
        end 
        [fname, dir] = uigetfile('*.*','MultiSelect','on');
        fn = fullfile(dir, fname);
    else 
        fn = allfn{k};
        [dir,~] = fileparts(fn);
    end 
    DIR{k} = dir;
    
    %% loading k-space from ScanArchive
    % IDEAL IQ: 'ReversePolarityEvenEchoes' = false
    % DEGRE: 'ReversePolarityEvenEchoes' = true
    % MEFGRE: 'ReversePolarityEvenEchoes' = false
    if nb_echoes == 2
        PolarityInput = true;
    else 
        PolarityInput = false;
    end 
    [kspace, info_out] = ScanArchive_import_multi_echo(fn, 'ReversePolarityEvenEchoes', PolarityInput, 'ReorderEchoes', true);    
    kspace = permute(kspace, [1 2 4 3 5]); % [x y z channels contrasts]

    dim_slices = 3;
    dim_channels = 4; 
    dim_contrasts = 5; % echoes
    
    if dimensions == 2 % only for ME-FGRE acquisitions
    %for data where the dimensions x y are not square, fill space with 0s
        if size(kspace,1) ~= size(kspace,2)
            size_kspace = size(kspace);
            nZerosToAdd = abs(size(kspace,2)-size(kspace,1));
            kspace = [zeros([nZerosToAdd size_kspace(2:end)]) ; kspace];
        end

        % re-ordering slices (this seems to always be needed)
        % order of slices now = 1 4 2 5 3, dimension slices = 3
        idx = [1:2:size(kspace,3) 2:2:size(kspace,3)];
        kspace(:,:,idx,:,:) = kspace;

        %% recon images:
        % ifft (X,[],DIM) is the inverse discrete Fourier transform
        % fftshift shifts zero-frequency component to center of spectrum

        % 2D
        fftfun = @(k) fftshift( ifft( ifft( k ,[], 1),[], 2), 1);
%         fftfun = @(k) fftshift( fftshift( ifft( ifft( k ,[], 1),[], 2), 2), 1); %TFN needed for DE-GRE

        cplxImages = fftfun( fftshift( fftshift( kspace, 1), 2)) ;

    
    elseif dimensions == 3 % for 3D aquisitions
        idx = [5:size(kspace,3) 1:4]; %%FFT assumes to be centred round 0
        kspace = kspace(:,:,idx,:,:);

        fftfun = @(k) fftshift( fftshift( ifft( ifft (ifft ( k ,[], 1),[], 2),[],3), 1), 2);
        cplxImages = fftfun(fftshift(fftshift(kspace,1),2));    
    end
    
%% coil combination for multi-coil acquisitions
    if nb_coils > 1
        %% Compute Coil sensitivity maps:
        % numEchoes = size( cplxImages, dim_echoes);
        % C = simplecoilest( permute( cplxImages , [1 2 4 3 5] ) , numEchoes<10 , 1 );
        
        if exist(fullfile(dir,'Cmaps.mat'), 'file') % check if Cmaps has already been created
            tmp = load(fullfile(dir,'Cmaps.mat'));
            Cmaps = tmp.Cmaps;
            
            if nb_echoes < 2
                szkspace = [size(kspace) 1];  % expand to always have the dim_constrast element present
            else 
                szkspace = size(kspace);
            end
        else
            coilopt = struct;
            coilopt.ncoilsets = 1;
            if nb_echoes < 2
                szkspace = [size(kspace) 1];  % expand to always have the dim_constrast element present
            else 
                szkspace = size(kspace);
            end
            
            if dimensions == 2
                coilopt.calibrationbox = [round( [.3;.7]*szkspace(1:2)) [1;1]]; % 3D box selecting the coil sensitivity calibration region inside kspace. 
            elseif dimensions == 3
                coilopt.calibrationbox = [round( [.3;.7]*szkspace(1:2)) [1;16]]; % 3D box selecting the coil sensitivity calibration region inside kspace. 
%               box size [53 53 16 ... ...] because slice 5-12 is zero  
%                 coilopt.calibrationbox = [round( [.3;.6]*szkspace(1:2)) [4;13]]; % 3D box selecting the coil sensitivity calibration region inside kspace. 
                % box size [40 40 10 ... ...]
            end 
            
            coilopt.notSampledIdentifier = nan;

        

            %% coil sensitivity stays constant for different echoes
            % 2D only, 3D can only call espirit coils once
            if dimensions == 2
                % allocate arrays
                vmaps = cell(1, szkspace(dim_slices)); 
                emaps = vmaps; 
                noise_singularvalues = vmaps; 
                sens = vmaps; 
                weight = vmaps; 
            
                for sl = 1:size(kspace, dim_slices) % iterate over slices
                    [vmaps{sl}, emaps{sl}, noise_singularvalues{sl}, sens{sl}, weight{sl}] = get_ESPIRiT_coils({ kspace(:,:,sl,:,:) }, fftfun, coilopt);
                end
                Cmaps = cat(3, sens{:}); % [x y z channels]

            elseif dimensions == 3
                [vmaps, emaps, noise_singularvalues, Cmaps, weight] = get_ESPIRiT_coils({kspace}, fftfun, coilopt);
            end
            
            fileNameSave = fullfile(dir, 'Cmaps.mat');
            save(fileNameSave, 'Cmaps');
        end

        %% Consistency between coil sensitivity maps 
        % if not equal will lead to phase differences between images. Can 
        % 1) use same coil sensitivity map at TP1 OR
        % 2) align phases of different maps by taking the angle of dot product
        % *coil sensitivity 
    %     Cmaps_ref %reference coil sensitivity map, TP 1
    %     Cmaps_cur_org %current coil sensitivity map at TP X

        if k == 1
            Cmaps_ref = Cmaps;
            Cmaps_cur = Cmaps;
        else 
            Cmaps_cur_org = Cmaps;
            phadj = exp(-1i*angle(dot(Cmaps_ref, Cmaps_cur_org, dim_channels))); 
            Cmaps_cur = bsxfun(@times, Cmaps_cur_org , phadj); %compensating arbitrary phase difference 
        end

        % From D. Poot proof/test of negative sign in the exponential of phadj:
        %
        % Cmaps_ref = [1 1i -1 -1i];
        % Cmaps_cor_org = 1i* Cmaps_ref;
        % dim_channels = 2; 
        % phadj = exp(-1i * angle( dot( Cmaps_ref, Cmaps_cor_org, dim_channels )) );
        % Cmaps_cur = bsxfun( @times, Cmaps_cor_org, phadj );


        %% Coil combine images:
        combinedImage = dot(repmat(Cmaps_cur, [1 1 1 1 szkspace(dim_contrasts)]), cplxImages, dim_channels);

        combinedImages(:,:,:,:,:,k) = combinedImage;
%         figure, imagebrowse(combinedImages)
    else 
        combinedImages(:,:,:,:,:,k) = cplxImages;
    end 
    % adding timestamps
    AT = info_out.DateTime(13:20);
    AqTime{k} = AT;
    AqTime_sec(k) = str2double(AT(1:2))*3600 + str2double(AT(4:5))*60 + ...
        str2double(AT(7:8));

end 
%sorting combinedImages timepoints by time of acquisition
[~, isort] = sort(AqTime_sec);
combinedImages = combinedImages(:,:,:,:,:,isort);
AqTime_sec = AqTime_sec(isort);
AqTime = AqTime(isort);

echotimes = info_out.TE(1:nb_echoes)';

TE{1, nb_timepoints} = [];
for jj = 1:nb_timepoints
    TE{1, jj} = echotimes; 
end 

nb_slices = size(combinedImages,3);

 %% rotating the reconstructed space
    if info_out.ArchiveInfo.Orientation.LegacyRotationType == 3
        for ii = 1:nb_slices
            for jj = 1:nb_echoes
                for kk = 1:nb_timepoints
                    combinedImages(:,:,ii,1,jj,kk) = imrotate(combinedImages(:,:,ii,1,jj,kk), 270);
                end
            end
        end
    end
    
    %flipping 2nd echo when readout is bipolar
    if info_out.polarity == 8 %only for DEGRE ???
%         if nb_echoes > 2
    %         for Multi-channel my phantom + DEGRE phantom
            TEMP = combinedImages;
            TEMP(:,65:end,:,:,:,:) = combinedImages(:,1:64,:,:,:,:);
            TEMP(:,1:64,:,:,:,:) = combinedImages(:,65:end,:,:,:,:);      
            combinedImages = TEMP;
%         end 
%         for ???
%         combinedImages(:,:,:,:,2,:) = combinedImages(end:-1:1,:,:,:,2,:);
    end
end
