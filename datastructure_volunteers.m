% Data structure for volunteer experiments Theresa:
% Includes data for 10 volunteers 
% Supplies information about scan in fields: 
%   filename
%   displayname
%   nechoes (2 for DE and 11 for ME)
%   Bodypart (Brain or Head and Neck)
%   input_type (ScanArchive or DICOM)
%
% all that information is stored in allfn and can be fed into import data
% functions
%
%   subjidx: 1-10 = number of volunteer, in chronological order
%   scanidx: 1-8 =  1:4 are Brain scans
%                   5:8 are Head and Neck scans 
%   they are ordered by time of acquisition ie always:
%                   DEGRE TR 620
%                   DEGRE TR 200
%                   MEGRE
%                   IDEAL IQ
%
% 26-8-2021, D.Poot, Erasmus MC: created. 
% 1-9-2021, T.Feddersen edited
% 13-3-2023, T.Feddersen added following thresholds

% fatEcho = 2;
% signalFractionThreshold = 0.07;
% water_threshold = 4;
% noise_threshold = 0.1;
% threshold_residual_outliers = 1000;


%% General structure of single scan session:
scanfields = { 'filename', 'displayname', 'nechoes', 'Bodypart', 'input_type','fatEcho', 'signalFractionThreshold','water_threshold', 'noise_threshold', 'threshold_residual_outliers'}; % add all things that (may) need to be specified differently for each scan
scans = cell2struct( { '' , 'DE-GRE_TR620', 2, 'Brain', 'ScanArchive', 2, 0.07, 4, 0.2, 1000;
                       '' , 'DE-GRE_TR200', 2, 'Brain', 'ScanArchive', 2, 0.07, 4, 0.2, 1000;
                       '' , 'MEFGE', 11', 'Brain', 'DICOM', 2, 0.07, 4, 0.2, 1000;
                       '' , 'IDEAL_IQ', 11, 'Brain', 'DICOM', 2, 0.07, 4, 0.2, 1000;
                       '' , 'DE-GRE_TR620', 2, 'Head & Neck', 'ScanArchive', 2, 0.07, 4, 0.2, 1000;
                       '' , 'DE-GRE_TR200', 2, 'Head & Neck', 'ScanArchive', 2, 0.07, 4, 0.2, 1000;
                       '' , 'MEFGE', 11', 'Head & Neck', 'DICOM', 2, 0.07, 4, 0.2, 1000;
                       '' , 'IDEAL_IQ', 11, 'Head & Neck', 'DICOM', 2, 0.07, 4, 0.2, 1000;
                }, scanfields , 2);
%             

%% Specify all scan sessions:
datasetbase = 'D:\Theresa-Home\volunteer_Data'; 
dataset = struct; 
vidx = 0 ; % volunteer index

%% volunteer # 1 
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_1'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
% Set filenames of first time point (in the order of scans, for all scans):
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20201207_112000863.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20201207_112306529.h5', ...
    '/MEFGRE_Brain_TP1/i1739879.MRDC.129', ...
    '/IDEAL_IQ_Brain_TP1/i1740275.MRDC.155', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20201207_115455587.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20201207_115737440.h5', ...
    '/MEFGRE_H&N_TP1/i1744559.MRDC.177', ...
    '/IDEAL_IQ_H&N_TP1/i1745935.MRDC.595');
% Set filenames of second time point (in the order of scans, for all scans):
[dataset( vidx ).scans(:,2).filename ] = deal( '/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20201207_114014783.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20201207_114207569.h5', ...
    '/MEFGRE_Brain_TP2/i1740947.MRDC.129', ...
    '/IDEAL_IQ_Brain_TP2/i1742367.MRDC.159', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20201207_120726385.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20201207_120926037.h5', ...
    '/MEFGRE_H&N_TP2/i1749471.MRDC.177', ... 
    '/IDEAL_IQ_H&N_TP2/i1750858.MRDC.595');
                  
%% volunteer # 2
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_2'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans]; % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210413_081636104.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210413_081901147.h5', ...
    '/MEFGRE_Brain_TP1/i3171493.MRDC.13', ...
    '/IDEAL_IQ_Brain_TP1/i3171834.MRDC.155', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210413_084800122.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210413_085000092.h5', ...
    '/MEFGRE_H&N_TP1/i3175963.MRDC.133', ...
    '/IDEAL_IQ_H&N_TP1/i3177239.MRDC.601');
[dataset( vidx ).scans(:,2).filename ] = deal( '/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210413_083355825.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210413_083550672.h5', ...
    '/MEFGRE_Brain_TP2/i3172486.MRDC.13', ...
    '/IDEAL_IQ_Brain_TP2/i3172828.MRDC.159', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210413_090709815.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210413_090851266.h5', ...
    '/MEFGRE_H&N_TP2/i3180575.MRDC.97', ...
    '/IDEAL_IQ_H&N_TP2/i3183131.MRDC.601');

%% volunteer # 3
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_3'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210413_121236334.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210413_121506822.h5', ...
    '/MEFGRE_Brain_TP1/i3190020.MRDC.70', ...
    '/IDEAL_IQ_Brain_TP1/i3190361.MRDC.163', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210413_124121455.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210413_124343146.h5', ...
    '/MEFGRE_H&N_TP1/i3194490.MRDC.82', ...
    '/IDEAL_IQ_H&N_TP1/i3195766.MRDC.597');
[dataset( vidx ).scans(:,2).filename ] = deal( '/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210413_122826541.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210413_123012590.h5', ...
    '/MEFGRE_Brain_TP2/i3191013.MRDC.85', ...
    '/IDEAL_IQ_Brain_TP2/i3191356.MRDC.167', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210413_125952923.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210413_130131043.h5', ...
    '/MEFGRE_H&N_TP2/i3199102.MRDC.97', ...
    '/IDEAL_IQ_H&N_TP2/i3201570.MRDC.595');

%% volunteer # 4
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_4'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210413_134128558.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210413_134349068.h5', ...
    '/MEFGRE_Brain_TP1/i3207267.MRDC.97', ...
    '/IDEAL_IQ_Brain_TP1/i3207608.MRDC.155', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210413_141041707.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210413_141241841.h5', ...
    '/MEFGRE_H&N_TP1/i3211737.MRDC.97', ...
    '/IDEAL_IQ_H&N_TP1/i3214293.MRDC.603');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210413_135726832.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210413_135924713.h5', ...
    '/MEFGRE_Brain_TP2/i3209540.MRDC.76', ...
    '/IDEAL_IQ_Brain_TP2/i3209881.MRDC.155', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210413_142914656.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210413_143054783.h5', ...
    '/MEFGRE_H&N_TP2/i3217629.MRDC.1', ...
    '/IDEAL_IQ_H&N_TP2/i3220185.MRDC.599');

%% volunteer # 5
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_5'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210419_154709829.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210419_154916355.h5', ...
    '/MEFGRE_Brain_TP1/i3290317.MRDC.97', ...
    '/IDEAL_IQ_Brain_TP1/i3290660.MRDC.169', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210419_161519665.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210419_161712371.h5', ...
    '/MEFGRE_H&N_TP1/i3293507.MRDC.79', ...
    '/IDEAL_IQ_H&N_TP1/i3295989.MRDC.595');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210419_160243752.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210419_160449165.h5', ...
    '/MEFGRE_Brain_TP2/i3291310.MRDC.82', ...
    '/IDEAL_IQ_Brain_TP2/i3291652.MRDC.175', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210419_163241670.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210419_163419371.h5', ...
    '/MEFGRE_H&N_TP2/i3299399.MRDC.97', ...
    '/IDEAL_IQ_H&N_TP2/i3300675.MRDC.603');

%% volunteer # 6
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_6'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210423_165009080.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210423_165212850.h5', ...
    '/MEFGRE_Brain_TP1/i3367484.MRDC.133', ...
    '/IDEAL_IQ_Brain_TP1/i3367826.MRDC.155', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210423_171708216.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210423_171843858.h5', ...
    '/MEFGRE_H&N_TP1/i3370674.MRDC.136', ...
    '/IDEAL_IQ_H&N_TP1/i3373230.MRDC.595');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210423_170443518.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210423_170623184.h5', ...
    '/MEFGRE_Brain_TP2/i3368477.MRDC.97', ...
    '/IDEAL_IQ_Brain_TP2/i3368823.MRDC.169', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210423_173417289.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210423_173552201.h5', ...
    '/MEFGRE_H&N_TP2/i3376566.MRDC.136', ...
    '/IDEAL_IQ_H&N_TP2/i3377767.MRDC.595');

%% volunteer # 7
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_7'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210504_130840890.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210504_131043986.h5', ...
    '/MEFGRE_Brain_TP1/i3495672.MRDC.136', ...
    '/IDEAL_IQ_Brain_TP1/i3496014.MRDC.157', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210504_133622213.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210504_133810358.h5', ...
    '/MEFGRE_H&N_TP1/i3498842.MRDC.133', ...
    '/IDEAL_IQ_H&N_TP1/i3501398.MRDC.599');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210504_132341954.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210504_132531818.h5', ...
    '/MEFGRE_Brain_TP2/i3496645.MRDC.7', ...
    '/IDEAL_IQ_Brain_TP2/i3496986.MRDC.159', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210504_135331773.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210504_135508599.h5', ...
    '/MEFGRE_H&N_TP2/i3504734.MRDC.136', ...
    '/IDEAL_IQ_H&N_TP2/i3506010.MRDC.597');

%% volunteer # 8
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_8'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210511_144103998.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210511_144322072.h5', ...
    '/MEFGRE_Brain_TP1/i3557784.MRDC.16', ...
    '/IDEAL_IQ_Brain_TP1/i3558126.MRDC.159', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210511_150747837.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210511_150951895.h5', ...
    '/MEFGRE_H&N_TP1/i3560974.MRDC.136', ...
    '/IDEAL_IQ_H&N_TP1/i3563530.MRDC.595');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210511_145549067.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210511_145726845.h5', ...
    '/MEFGRE_Brain_TP2/i3558777.MRDC.31', ...
    '/IDEAL_IQ_Brain_TP2/i3559120.MRDC.165', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210511_152506284.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210511_152641400.h5', ...
    '/MEFGRE_H&N_TP2/i3566866.MRDC.133', ...
    '/IDEAL_IQ_H&N_TP2/i3568142.MRDC.595');

%% volunteer # 9
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_9'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210514_173132518.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210514_173342914.h5', ...
    '/MEFGRE_Brain_TP1/i3613094.MRDC.13', ...
    '/IDEAL_IQ_Brain_TP1/i3613437.MRDC.161', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210514_180225947.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210514_180405568.h5', ...
    '/MEFGRE_H&N_TP1/i3616284.MRDC.76', ...
    '/IDEAL_IQ_H&N_TP1/i3618840.MRDC.597');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210514_174905102.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210514_175058556.h5', ...
    '/MEFGRE_Brain_TP2/i3614087.MRDC.88', ...
    '/IDEAL_IQ_Brain_TP2/i3614431.MRDC.165', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210514_181930653.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210514_182109698.h5', ...
    '/MEFGRE_H&N_TP2/i3622176.MRDC.16', ...
    '/IDEAL_IQ_H&N_TP2/i3623390.MRDC.595');

%% volunteer # 10
vidx = vidx+1; % add new entry
dataset( vidx ).basefolder = 'volunteer_10'; % subfolder of datasetbase
dataset( vidx ).scans = [scans scans];  % second dimension is time points. 
[dataset( vidx ).scans(:,1).filename] = deal('/DEGRE620_Brain_TP1/ScanArchive_RTD0424_20210517_130515715.h5', ...
    '/DEGRE200_Brain_TP1/ScanArchive_RTD0424_20210517_130721279.h5', ...
    '/MEFGRE_Brain_TP1/i3633188.MRDC.97', ...
    '/IDEAL_IQ_Brain_TP1/i3633529.MRDC.155', ...
    '/DEGRE620_H&N_TP1/ScanArchive_RTD0424_20210517_133208220.h5', ...
    '/DEGRE200_H&N_TP1/ScanArchive_RTD0424_20210517_133345165.h5', ...
    '/MEFGRE_H&N_TP1/i3636378.MRDC.67', ...
    '/IDEAL_IQ_H&N_TP1/i3638934.MRDC.599');
[dataset( vidx ).scans(:,2).filename ] = deal('/DEGRE620_Brain_TP2/ScanArchive_RTD0424_20210517_131943015.h5', ...
    '/DEGRE200_Brain_TP2/ScanArchive_RTD0424_20210517_132123197.h5', ...
    '/MEFGRE_Brain_TP2/i3634181.MRDC.85', ...
    '/IDEAL_IQ_Brain_TP2/i3634524.MRDC.167', ...
    '/DEGRE620_H&N_TP2/ScanArchive_RTD0424_20210517_134846680.h5', ...
    '/DEGRE200_H&N_TP2/ScanArchive_RTD0424_20210517_135023759.h5', ...
    '/MEFGRE_H&N_TP2/i3642270.MRDC.133', ...
    '/IDEAL_IQ_H&N_TP2/i3643546.MRDC.595');


%% Extract information needed for current scans:
curbase = fullfile( datasetbase, dataset( subjidx ).basefolder );
allfn = cell( 1, size( dataset( subjidx ).scans, 2 ));
for t = 1 : numel( allfn )
    allfn{t} = fullfile( curbase, dataset( subjidx ).scans(scanidx, t).filename );
end
nechoes = dataset( subjidx ).scans(scanidx, 1).nechoes;
nchannels = 22;
ntimepoints = 2;
if strcmp(dataset(subjidx).scans(scanidx,1).input_type,'ScanArchive')
    ScanArchive = 1;
else 
    ScanArchive = 0; 
end 

