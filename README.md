# MR thermometry

MATLAB functions used to evaluate MR thermometry in my project for phantoms and volunteers.

## Description

My project is developing reliable and rubust MR thermometry for monitoring mild hyperthermia treatments in the clinic, using the head & neck hyperthermia applicator (with integrated receive coils) that has just been developed by our group.

This code so far has only been optimised for MRT in phantom and for the brain of healthy volunteers, it has not been tested in patients, nor has any clinical validation of any form taken place. 

With modifications, this pipeline can most likely be used for other areas of the body.

General structure MRT_phantom: 
 1. Loads data from ScanArchives or dicoms
 2. Calculates change in offresonance 
 3. Applies B0 drift correction by selecting external fat tubes (automatically OR manually)
 4. Calculates the relative temperature change with PRFS
 5. Imports "true" temperature data from BSD probes
 6. calculates MRT performance: Bias, spatial temperature precision, temporal temperature precision and accuracy

General structure MRT_volunteers:
 1. Loads data from datastructure
 2. Performs complex image registration and transformation using Elastix
 3. Calculates change in offresonance maps with WF-fitting tool (for multi-echo acquisitions)
 4. Applies B0 drift correction using automatically selected internal body fat
 5. Calculates the relative temperature change with PRFS
 6. extracts MRT performance matrices: SD and mean (absolute mean = stability)


## Getting Started

### Dependencies

This code was made on MATLAB 2018b and it might not run on a different version.
All function dependencies that include code that is property of GE Healthcare have been excluded (such as import_coilCombine_multiEcho.m).

### Installing

The code can be downloaded from this site, no modifications need to be made apart from some default paths set at the start, as well as of course a new data structure, should the user want to use his/her own data.
This code only works using DICOMS, even though for some of our evaluation ScanArchives were used and the k-space was reconstructed manually. However, since any code using functions to access the ScanArchives are part of Orchestra, and therefore GE Healthcare, we are not able to share those here. 

### Executing program

* Depending on what needs evaluating, start with opening MRT_phantom.m (for phantom data) or MRT_volunteer.m (for volunteer data)
* It's best to run the code section by section, since certain parts of the code give options for the user; the desirable one may be commented out



## Help

MATLAB will provide details on where the problem is. With most issues, the help text of the individual scripts and functions should be able to clear up the problem. 


## Authors

Contributors names and contact info

Theresa Feddersen 
t.feddersen@erasmusmc.nl

Dirk Poot
d.poot@erasmusmc.nl


## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the Apache License, Version 2.0 - see the LICENSE.md file for details

## Acknowledgments

