function [w0, rho_w, rho_f, phi0, R2star, prediction, CRLB] = WF_fitting(TE, FatPeakRatio, FatFreq, img_r, mask) 
% [w0, rho_w, rho_f,phi0,R2star, prediction] = WF_fitting(TE, FatPeakRatio, FatFreq, img_r, mask) 
% OR:
% [w0, rho_w, rho_f,phi0,R2star, prediction] = WF_fitting(TE, SFat, [], img_r, mask) 
%   
% This function fits the water and fat model to multi echo gradient echo
% images
%
% INPUTS:
% TE  : n element column vector with echo time in sec of all the images
% FatPeakRatio : m element column vector with the fractional contribution of each fat peak. 
% FatFreq      : m element column vector with the offresonance frequency of
%                each fat peak in rad/sec 
% SFat    : complex fat signal (at a position with zero offresonance of water). 
% img_r  : 2n x spatialsize real valued images;  [real;imag] of n complex
%          valued gradient echo images acquired with the echo times
%          specified in TE. 
% mask : mask in which the fitting is performed.
% OUTPUTS:
%  w0, rho_w, rho_f,phi0,R2star : spatialsize size maps containing, respectively, the 
%           offresonance (rad/s), scaled water protondensity, scaled fat
%           protondensity, phase at TE=0 (rad), R2* (1/s)
%           These maps are the result of the maximumlikelihood
%           optimization. 
%  prediction : the predicted  img_r, using the model and the maps that are
%               outputted. 
%
% Created by Ghassan Salim, in collaboration with Dirk Poot, Erasmus MC.
% before March 2016
% Fitting made significantly more robust and faster by Dirk Poot, Erasmus MC, April 2016


%% Construct function
if ~isreal(FatPeakRatio)
    SFat = FatPeakRatio; 
else
    SFat = makeSFat(TE, FatPeakRatio, FatFreq );
end;
fun = @(theta) pred_MultiEchoGE_WaterAndFat_phi0(theta, TE, SFat);
% fun2 =  @(img) pred_MultiEchoGE_WaterAndFat_phi0(theta, TE, FatPeakRatio, FatFreq);
% fun = @(theta, img) pred_MultiEchoGE_WaterAndFat_phi0(theta, TE, FatPeakRatio, FatFreq);
% TE_many = linspace(0,max(TE),100)'; 
% fun_many =  @(theta) pred_MultiEchoGE_WaterAndFat_phi0(theta, TE_many, FatPeakRatio, FatFreq);

%% validate function:
% validateJacobian(fun, [1.34;1.123;3.234;0.1;0.1])

%% Values Initialization
% mask = abs(img_c(1,:,:,:))>.15;

%% Compute estimated values
fitopt = struct;
fitopt.imageType = 'real';
% fitopt.noiseLevel = sigmaForCRLB;
fitopt.noiseLevel = .1;
fitopt.blockSize = [2 2 1];
fitopt.mask = mask ;
% fitopt.initialValueSpecifierVect = [0 0 0 0 10000];
fitopt.initialValueSpecifierVect = [0 0 0 true];
sTE = sort(TE);
offresinterval = .7*pi/mean( diff(sTE) ); % max interval is -pi to pi/diff_TE, use a bit smaller to save on initialization room
maxR2 = 1/max(2*sTE(1), sTE(2) );
% fitopt.initialValueRange = [0 1;0 1;5 maxR2;-[1 1]*offresinterval;-pi pi];

nphasesteps = 6; % 6 steps in 180 degrees => 30 degree steps => average error <30 degrees. 
                 % NOTE: number of initial points scales quadratically with nphasesteps
% (sTE(end)-sTE(1)) * init_w0step == pi/nphasesteps =>
init_w0step = pi/(nphasesteps*(sTE(end)-sTE(1)));
init_R2star = [1 3]./(sTE(end)-sTE(1)); 
[initrange_phi0, initrange_w0, initrange_R2star] = ndgrid( (1:nphasesteps)/nphasesteps*pi, -offresinterval:init_w0step:offresinterval, init_R2star);
fitopt.extraInitialValues = mat2cell([ones(2,numel(initrange_phi0));
                                      reshape(initrange_R2star,1,[]);
                                      reshape(initrange_w0,1,[]);
                                      reshape(initrange_phi0,1,[])],5,ones(1,numel(initrange_phi0)) ) ;
fitopt.linearParameters = [1 2];
fitopt.optimizer = 3;
thtinit = [1;1;25;0;0];
%% fitopt.maxIter=0;

thtest = fit_MRI( fun, img_r, thtinit ,fitopt);
% thtest2 = fit_MRI( fun2, img_r, thtinit ,fitopt);

if 0 
    %% inspect residual:
    pred = reshape( fun(thtest(:,:) ) , size(img_r) );
    resid = img_r-pred;
    comb = cat(5,img_r,pred,resid);
    combc = complex(comb(1:numel(TE),:,:,:,:),comb(numel(TE)+1:end,:,:,:,:));
    imagebrowse(combc);
    %% try to improve fit:
    fitopt2 = rmfield(fitopt,{'initialValueSpecifierVect','extraInitialValues'})
    fitopt2.startBlockPos = [0 0];
    thtest2 = fit_MRI( fun, img_r, thtest ,fitopt2);
end;
% Change sign of negative (net) proton densities. Predict same signal by adjusting phi0
adj = (thtest(1,:)+thtest(2,:))<0; 
thtest(1:2,adj) = - thtest(1:2,adj);
thtest(5,adj) = thtest(5,adj)+pi; 

% extract parameters:
rho_w = permute( thtest(1,:,:,:) ,[2 3 4 5 1] );
rho_f = permute( thtest(2,:,:,:) ,[2 3 4 5 1] );
R2star = permute( thtest(3,:,:,:) ,[2 3 4 5 1] );
w0 = permute( thtest(4,:,:,:) ,[2 3 4 5 1] );
phi0 = permute( thtest(5,:,:,:) ,[2 3 4 5 1] );
if nargout>=6
    prediction = reshape( fun(thtest(:,:) ) , size(img_r) );
    if nargout>=7
        sigma = .05;
        [CRLB, I,J]= CramerRaoLowerBound_MRI( thtest, fun, sigma,'real');
    end;
end;
