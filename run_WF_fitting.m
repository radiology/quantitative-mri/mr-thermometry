function [offres, rho_w, rho_f, fit_mask] = run_WF_fitting(I1, TE, dicom_time_sec, dir)
% Imports complex data and feeds it into WF_fitting function.  
% Saves and outputs the off-resonance-, fat- & water-map and fit_mask
%
% INPUTS:
%   I1 = complex values input data [x y slices coils echoes timepoints]
%   TE = n element column vector with echo time of all the images (s)
%   dicom_time_sec = column vector with aquisition time in seconds of all the images
%   dir = n element cell with directory of raw data
%
% OUTPUTS:
%   offres = offresonance map (rad/s) [x y slices timpepoints]
%   rho_w = scaled water proton density [x y slices timepoints]
%   rho_f = scaled fat proton density [x y slices timepoints]
%   fit_mask = logical mask where magniture of I1 > signal threshold (now = 100) 

%for head& neck
% signal_threshold = 100; %change this value to correct the mask


% 03-02-2022 Theresa F edited changing saving parameters procedure

signal_threshold = 0.5;
FatPeakRatio = [.62 .15 .10 .06 .03 .04]';
FatFreq = (2*pi)*[210 159 -47 236 117 23]'; % in rad/s for a 1.5T scanner

nb_timepoints = size(I1, 6);
nb_slices = size(I1, 3);
nb_echoes = size(I1, 5);

offres = zeros(size(I1,1),size(I1,2),nb_slices,nb_timepoints);
rho_f = zeros(size(I1,1),size(I1,2),nb_slices,nb_timepoints);
rho_w = zeros(size(I1,1),size(I1,2),nb_slices,nb_timepoints);
fit_mask = false(size(I1,1),size(I1,2),nb_slices,nb_timepoints);

for jj = 1:nb_timepoints
    fprintf('Evaluating timepoint %d \n', jj);
    for ii = 1:nb_slices
        for kk = 1:nb_echoes
            img_part(kk,:,:,ii) = real(I1(:,:,ii,1,kk,jj)); %real image
            img_part(kk+nb_echoes,:,:,ii) = imag(I1(:,:,ii,1,kk,jj)); %imaginary image
        end 
    end 
    
    clear mask
    img_r{jj} = img_part;
    for ii = 1:nb_slices
        mask1 = zeros(size(I1,1),size(I1,2));
        ind_ind = find(abs(I1(:,:,ii,1,1,jj)) > signal_threshold);
        mask1(ind_ind) = 1;
        mask(1,:,:,ii) = mask1;
    end
    mask = logical(mask);
    fit_mask(:,:,:,jj) = mask;
    %[fitted_data(jj).offresonance, fitted_data(jj).rho_w, fitted_data(jj).rho_f, fitted_data(jj).phi0, fitted_data(jj).R2star, fitted_data(jj).prediction] = WF_fitting(TE{jj}, FatPeakRatio, FatFreq, img_r{jj}, mask);
    [fitted_data(jj).offresonance, fitted_data(jj).rho_w, fitted_data(jj).rho_f,~,~,~] = WF_fitting(TE{jj}, FatPeakRatio, FatFreq, img_r{jj}, mask);
    offres(:,:,:,jj) = fitted_data(jj).offresonance; 
    rho_f(:,:,:,jj) = fitted_data(jj).rho_f;
    rho_w(:,:,:,jj) = fitted_data(jj).rho_w;

end

[~, isort] = sort(dicom_time_sec);
offres = offres(:,:,:,isort); 
rho_f = rho_f(:,:,:,isort);
rho_w = rho_w(:,:,:,isort);


fileName = fullfile(dir{nb_timepoints}, 'offres.mat');
save(fileName, 'offres');
fileName2 = fullfile(dir{nb_timepoints}, 'rho_f.mat');
save(fileName2, 'rho_f');
fileName3 = fullfile(dir{nb_timepoints}, 'rho_w.mat');
save(fileName3, 'rho_w');
fileName4 = fullfile(dir{nb_timepoints}, 'fit_mask.mat');
save(fileName4, 'fit_mask');
    end