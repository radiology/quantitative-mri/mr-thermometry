function SFat = makeSFat( TE, FatPeakRatio,FatFreq )
% INPUTS
% TE  : 
% FatPeakRatio : m element column vector with the fractional contribution of each fat peak. 
%                default: FatPeakRatio= [.62 .15 .10 .06 .03 .04];
% FatFreq      : m element column vector with the offresonance frequency of
%                each fat peak in rad/sec.
%                default:  FatFreq= (1/(2*pi))*[420 318 -94 472 234 46]; % in rad/s
% OUTPUTS:
% SFat : complex signal of fat for these TE. 
%
% 16-6-2017: Created by Dirk Poot, Erasmus MC. Extracted from WF_fitting.m


if nargin<2 || isempty( FatPeakRatio )
    FatPeakRatio= [.62 .15 .10 .06 .03 .04];
end;
if nargin<3 || isempty( FatFreq )
    FatFreq= (1/(2*pi))*[420 318 -94 472 234 46]; % in rad/s
end;
% Compute complex valued images:
nb_peaks = numel(FatFreq); % nb of fat peaks

nTE= numel(TE);

SfatPeaks = zeros(nTE, nb_peaks); % stores signal for each individual fat peak for a given TE.

for i=1:numel(TE)   
    for j=1:nb_peaks
        SfatPeaks(i,j) = FatPeakRatio(j) * exp(1i * FatFreq(j) * TE(i));
    end
end
SFat = sum(SfatPeaks,2); % sum of the signal of all the fat peaks for a given TE 
