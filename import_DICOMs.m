function [images, AqTime, AqTime_sec, TE_all, info, DIR] = import_DICOMs(allfn, nb_slices, nb_echoes, nb_timepoints, dimensions)
%    [combinedImages, AqTime, AqTime_sec, TE, info_out, Dir] = import_ME_data(allfn, nslices, nechoes, ntimepoints, dimensions);
%
% function to import single-channel and multi-echo data from DICOMs and return complex valued images
%
% INPUT: 
%   allfn = cell containing filenames of data to be loaded
%   nb_slices = number of slices at acquisition
%   nb_echoes = number of echoes 
%   nb_timepoints = number of timepoints
%   dimensions = dimensions of input sequence (2 or 3)
%   
%
% OUTPUT:
%   images = complex valued data in the form: (x y nslices nechoes ncoils ntimepoints)
%   AqTime = acquisition time of dicoms
%   AqTime_sec = acquisition time of dicoms in seconds 
%   TE = echo times for all echoes (s)
%   info = dicominfo about scan
%   DIR = working directory, smallest level possible
%
% Theresa Feddersen, created 28-1-2022
% T Feddersen, edited 3-2-2022


test = dir(allfn{nb_timepoints});
if ~exist(fullfile(test.folder, 'combinedImages.mat'),'file')
    DIR{nb_timepoints} = [];
    AqTime{nb_timepoints} = [];
    AqTime_sec = zeros(nb_timepoints,1);

    for k = 1:nb_timepoints 
        fprintf('Evaluating timepoint %d \n', k);
        file = dir(allfn{k});
        folder_cur = file.folder;
        DIR{k} = folder_cur;
        allFiles = dir(fullfile(folder_cur, '*.dcm'));
        fileNames = {allFiles.name};

        % check whether enough images are present to build complex values. Need at least magnitude & phase OR real & imaginary 
        nFiles = size(fileNames,2);
        if nFiles < (2*nb_slices*nb_echoes)
            err_msg = 'The number of files is not enough to build complex valued files, terminating function. ';
            error(err_msg)
        end 

        filesRep = nFiles/(nb_slices*nb_echoes);

        for xx = 1:filesRep     
            for n = 1:nb_slices
                idx = 1 + (n-1)*filesRep*nb_echoes + (xx-1);
                index = idx;
                for m = 1:nb_echoes 
                    dicom_file(:,:,n,m) = dicomread(fullfile(folder_cur, fileNames{index}));
                    info = dicominfo(fullfile(folder_cur, fileNames{index})); % just to extract the echo times              
                    TE(m) = info.EchoTime.*10^(-3);    
                    index = index+filesRep;           
                end
            end
            
            %FOR DE + MEFGRE ONLY
            if dimensions == 2
            % tag   = 0 for mag 
            %       = 1 for phase 
            %       = 2 for real 
            %       = 3 for imag               
            info_type = dicominfo(fullfile(folder_cur,fileNames{idx}));
            Tag = info_type.Private_0043_102f;

            % assign variables depending on tag  
%             if Tag == 0
%                 mag(:,:,:,:,k) = double(dicom_file);
%             elseif Tag == 1 
%                 phase(:,:,:,:,k) = double(dicom_file); 
            if Tag == 2
                Re(:,:,:,:,k) = double(dicom_file);
            elseif Tag == 3
                Im(:,:,:,:,k) = double(dicom_file);
            end
        clear dicom_file   
        
        %IDEAL IQ RE+Im in folder one by one in order
            elseif dimensions == 3
                if xx == 1
                    Re(:,:,:,:,k) = double(dicom_file);
                elseif xx == 2 
                    Im(:,:,:,:,k) = double(dicom_file);
                end 
            end 
        clear dicom_file    

            
        end

        AT = info.AcquisitionTime;
        AqTime{k} = AT;
        AqTime_sec(k)= str2num(AT(1:2))*3600 + str2num(AT(3:4))*60 + str2num(AT(5:6));

        TE_all{k} = TE';
    end

    % resorting the timepoints according to time of acquisition
    [~, isort] = sort(AqTime_sec);
%     mag = mag(:,:,:,:,isort);
%     phase = phase(:,:,:,:,isort);

%     Test = Re;
%     Test(2:2:end, 1:2:end, :,:) = -Test(2:2:end, 1:2:end, :,:);
%     Test(1:2:end, 2:2:end, :,:) = -Test(1:2:end, 2:2:end, :,:);
%     Test(:,:,2:2:end,:) = -Test(:,:,2:2:end,:); % im has same flip here I think
%     Test(:,:,:,2:2:end) = - Test(:,:,:,2:2:end);
%     Re = Test;
% figure, imagebrowse(Test)
%     Complex = complex(Test,Im);
%     Complex(:,:,1:2:end,:) = -Complex(:,:,1:2:end,:);
%     figure, imagebrowse(Complex)
    
     %%            
    Re = Re(:,:,:,:,isort);
    Im = Im(:,:,:,:,isort);

    AqTime_sec = AqTime_sec(isort);
    AqTime = AqTime(isort);

%     images = mag.*exp(phase.*-1i/1000); 

% use this to avoid grad warp in phase images
    images = complex(Re, Im);
%     images(:,:,1:2:end,:) = -images(:,:,1:2:end,:); %to align slices

    images = reshape(images, [size(images,1), size(images,2), nb_slices, 1, nb_echoes, nb_timepoints]); %adding coils in 4th dimension

%     save sorted complex valued images and info 
    fileName = fullfile(test.folder, 'combinedImages.mat');
    save(fileName, 'images', '-v7.3');
    fileName = fullfile(test.folder, 'AqTime.mat');
    save(fileName, 'AqTime');
    fileName = fullfile(test.folder, 'AqTime_sec.mat');
    save(fileName, 'AqTime_sec');
    fileName = fullfile(test.folder, 'TE.mat');
    save(fileName, 'TE_all');
    fileName = fullfile(test.folder, 'Dir.mat');
    save(fileName, 'DIR');
    fileName = fullfile(test.folder, 'info_out.mat');
    save(fileName, 'info');

else 
    tmp = load(fullfile(test.folder, 'combinedImages.mat'));
    images = tmp.images;
    tmp = load(fullfile(test.folder, 'AqTime.mat'));
    AqTime = tmp.AqTime;
    tmp = load(fullfile(test.folder, 'AqTime_sec.mat'));
    AqTime_sec = tmp.AqTime_sec;
    tmp = load(fullfile(test.folder, 'TE.mat'));
    TE_all = tmp.TE_all;
    tmp = load(fullfile(test.folder, 'Dir.mat'));
    DIR = tmp.DIR;
    tmp = load(fullfile(test.folder, 'info_out.mat'));
    info = tmp.info;
end 

end