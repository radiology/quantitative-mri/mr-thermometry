function offresonance_change = doubleEcho_offresChange(images, TP_ref, TE, ~, varargin)
% offres_frequency = doubleEcho_offresChange(combinedImages, TP_ref, TE, info_out, varargin)
%
% Calculates the offresonance frequency change of DEGRE acquisitions using 
%  both echoes, unless otherwise specified in the function input 
% 
% INPUTS:
%   images = complex DEGRE images [x y slices coils echoes timepoints]
%   TP_ref = Time point to reference to (usually = 1)
%   TE = echo times in seconds
%   info_out = information from scanarchive, needs to contain both echo times
%
% options: varargin = 1 : using only first echo for calculations
%                   = 2 : using only second echo for calculations
%
% OUTPUTS: 
%   offres_frequency = offresonance frequency change between DEGRE 
%       acquisitions for all timepoints [x y slices coils 1 timepoints] (rad/s)
% 
% 13-01-2022: T Feddersen, created
% 18-01-2022: T. Feddersen, modified: changed TE handling  
% 01-02-2022: T. Feddersen, modifier: made compatible with DICOM input

% TODO: check whether input with TE still runs for scanarchives

nb_timepoints = size(images, 6);

% if ScanArchive == 1
% TE1 = info_out.TE(1);
% TE2 = info_out.TE(2);
% end

TE1 = TE{TP_ref}(1);
TE2 = TE{TP_ref}(2);

if nargin > 4
    chosen_echo = varargin{1};
    if chosen_echo == 1
        disp('using only first echo for calculations...')
        TE = TE1;
    elseif chosen_echo == 2
        disp('using only second echo for calculations...')
        TE = TE2;
    end 
    for n = 1:nb_timepoints
        offresonance_change(:,:,:,:,:,n) = angle(images(:,:,:,:,chosen_echo,n).*conj(images(:,:,:,:,chosen_echo,TP_ref)))/TE; % phase angle in the interval [-?,?] 
    end 

else
    
    tmp_phase_ref = images(:,:,:,:,2,TP_ref).*conj(images(:,:,:,:,1,TP_ref));

    for n = 1:nb_timepoints    
        tmp_phase = images(:,:,:,:,2,n).*conj(images(:,:,:,:,1,n));
        offresonance_change(:,:,:,:,:,n) = angle(tmp_phase.*conj(tmp_phase_ref))/(TE2-TE1); % phase angle in the interval [-?,?] 
    %     phase_change_corr = phase_change - 2*pi*round(phase_change/(2*pi));
    end
end 
end 

